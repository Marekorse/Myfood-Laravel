<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RecipesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\AdminController;;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ConversationsController;
use App\Http\Controllers\ratingsController;
use App\Http\Controllers\FavouriteRecipeController;
use App\Http\Controllers\LikesController;
use App\Http\Controllers\FollowersController;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\ConfigController;
use GuzzleHttp\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Settings(dev)
|--------------------------------------------------------------------------
*/
Route::middleware(['isadmin'])->group(function () {

     Route::get('/configclear', [ConfigController::class, 'resetConfig']);
     Route::get('/link', [ConfigController::class, 'link']);

});
/*
|--------------------------------------------------------------------------
| Guest
|--------------------------------------------------------------------------
*/
     Route::middleware(['verified'])->group(function () {

          /*
          Recipe
          */

               Route::get('/recept/novy', [RecipesController::class, 'create']);
               Route::post('/recept', [RecipesController::class, 'store']);
               Route::get('recepty', [RecipesController::class, 'index']);
               Route::get('/', [RecipesController::class, 'index']);
               Route::get('/recept/{id}', [RecipesController::class, 'show']);

          /*
          Rating
          */

               Route::get('/rating', [RatingsController::class, 'index']);
               Route::post('/rating', [RatingsController::class, 'store']);

          /*
          user
          */

               Route::get('/pouzivatel/{id}/{category?}/{page_no?}', [UsersController::class, 'show']);

          /*
          Comment
          */

               Route::resource('/comments', CommentsController::class)->only(['index'])->middleware('isAjax');
               
          /*
          Likes
          */

               Route::resource('/likes', LikesController::class)->only(['index']);

          /*
          followers
          */

               Route::resource('/followers', FollowersController::class)->only(['index']);
          
          

     });

/*
|--------------------------------------------------------------------------
| Auth
|--------------------------------------------------------------------------
*/

     Route::middleware(['isadmin', 'auth','verified'])->group(function () {
          /*
          todo
          */
               Route::resource('/todo', TodoController::class);
               Route::put('/task/reorder', [TasksController::class, 'reorder']);
               Route::resource('/task', TasksController::class);
               Route::resource('/user', UsersController::class);
          /*
          Admin
          */
               Route::get('/admin', [AdminController::class, 'index']);
               Route::get('/admin/stats', [AdminController::class, 'showStatsByDate']);
               Route::get('/admin/users', [AdminController::class, 'usersFilter']);
               Route::get('/admin/recipes', [AdminController::class, 'recipesFilter']);
               Route::put('/admin/recipe/{id}', [AdminController::class, 'recipeAdministration']);
               Route::delete('/admin/recipe/{id}', [AdminController::class, 'recipeAdministration']);
               Route::put('/admin/user/{id}', [AdminController::class, 'userAdministration']);
               Route::delete('/admin/user/{id}', [AdminController::class, 'deleteUser']);
     });

     Route::middleware(['auth','verified'])->group(function () {
          /*
          user
          */
          Route::get('/nastavenia', [UsersController::class, 'edit'])->middleware('verified');
          Route::resource('/user', UsersController::class)->only(['update', 'destroy',])->middleware('verified');
          Route::delete('/notification/all', [NotificationController::class, 'destroyAll']);
          Route::put('/notification', [NotificationController::class, 'markAsRead']);
          Route::resource('/notification', NotificationController::class)->only(['index', 'destroy']);

          /*
          Recipe
          */

               Route::resource('/recept', RecipesController::class)->only(['update', 'destroy', 'edit']);

          /*
          Conversation
          */
               Route::resource('/spravy', ConversationsController::class)->only('index');
               Route::resource('/conversation', ConversationsController::class)->only('store', 'show', 'destroy');
               Route::resource('/messages', MessagesController::class)->only('store', 'destroy');

          /*
          Favourite recipes
          */

               Route::delete('/favourite', [FavouriteRecipeController::class, 'delete']);
               Route::post('/favourite', [FavouriteRecipeController::class, 'store']);
               Route::get('/recepty/oblubene', [FavouriteRecipeController::class, 'index']);

          /*
          Comment
          */

               Route::resource('/comments', CommentsController::class)->only([
                    'store', 'destroy', 'update'
               ])->middleware('isAjax');

          /*
          Likes
          */

               Route::resource('/likes', LikesController::class)->only(['store', 'destroy']);

          /*
          followers
          */

               Route::resource('/followers', FollowersController::class)->only(['store', 'destroy']);

          /*
          Rating
          */

          Route::get('/rating/users', [RatingsController::class, 'UsersRatings']);

     });

require __DIR__ . '/auth.php';
