<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class taskpart extends Model
{
    use HasFactory;

    protected $fillable =[
        'part_name', 'deadline', 'img', 'created_at', 'updated_at'
    ];

    public function taskparts()
    {
        return $this->belongsTo(task::class);
    }
}
