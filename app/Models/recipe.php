<?php
namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recipe extends Model
{
    use HasFactory;

    protected $fillable =[
        'title', 'img', 'time', 'chunk', 'difficulty', 'amount', 'ingredients', 'text', 'approach',
        'slug', 'cretated_at', 'updated_at', 'confirmation', 'guest', 'category', 'popularity'
    ];

    protected $casts = [
        'ingredients' => 'array',
        'amount' => 'array',
        'approach' => 'array',
    ];

    /**
     * Get all of the comments for the recipe
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(comment::class);
    }

    /**
     * Get the recipes that owns the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function recipe()
    {
        return $this->belongsTo(User::class);
    }

    public function ratings()
    {
        return $this->hasMany(rating::class);
    }

    public function favourite()
    {
        return $this->belongsToMany(User::class);
    }

    public function likes()
    {
        return $this->hasMany(like::class);
    }
}
