<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class like extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $fillable = [
        'recipe_id'
    ];

    public function recipe()
    {
        return $this->belongsTo(recipe::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
