<?php
namespace App\Http\Controllers;

use App\Events\newConversation;
use App\Notifications\notifyUser;
use App\Models\User;
use App\Events\newMessage;
use App\Models\Conversation;
use App\Models\message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ConversationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $user = auth()->user();

            $conversations = $user->conversations()->with('users')->withCount(['messages' => function (Builder $query) use ($user) {
                $query->where([['read_at', '=', null], ['user_id', '<>', $user->id], ]);
            }])->latest()->simplePaginate(16);

            return response($conversations, 200);
        }
        return view('conversations');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'subject' => 'required| String',
            'text'    => 'required| String',
            'touser' => 'required| integer',
        ]);

        $user=auth()->user();

        $reciever = User::find($request->touser);

        $conversation = new Conversation();
        $conversation->subject = $request->subject;
        $conversation->save();

        $reciever->conversations()->attach($conversation->id);

        $user->conversations()->attach($conversation->id);

        $newmessage = $user->messages()->create([
            'conversation_id' => $conversation->id,
            'text' => $request->text
        ]);

        $message = 'vám poslal novú správu';

        $url = url('/spravy');

        $data = [
            'message' => $message,
            'icon'=> 'avatar',
            'user' => $user,
            'link' =>   $url
        ];

        $user->notify(new notifyUser($data));

        broadcast(new newMessage($newmessage->load('user')));


        broadcast(new newConversation($reciever->id,$conversation->load('users') ));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conversation  $conversation
     * @return \Illuminate\Http\Response
     */
    public function show(Conversation $conversation)
    {
        $this->authorize('view', $conversation);

        $messages = message::where('conversation_id', $conversation->id)->with('user')->simplePaginate(50);

        message::where([['user_id', '<>', auth()->id()], ['read_at', '=', null]])->update(['read_at' => now()]);

        return response($messages, 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conversation  $conversation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conversation $conversation)
    {
        $this->authorize('delete', $conversation);

        if (count($conversation->users) > 1) {
            auth()->user()->conversations()->detach($conversation->id);
        } else {
           return $conversation->delete();
        }
    }
}
