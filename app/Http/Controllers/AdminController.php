<?php

namespace App\Http\Controllers;

use App\Models\recipe;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Display view.
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin');
    }

    /**
     *show statistics by date.
     *@return Response
     */
    public function showStatsByDate(Request $request)
    {
        $day=$request->day;

        $month=$request->month;

        $year=$request->year;

        $models = ['visitor', 'user', 'recipe', 'notification'];

        foreach ($models as $model_name) {
            $model = 'App\Models\\' . $model_name;

            ${$model_name . 's'} = $model::when($day, function ($query, $day) {
                return $query->whereDay('created_at', $day);
            })
                ->when($month, function ($query, $month) {
                    return $query->whereMonth('created_at', $month);
                })
                ->when($year, function ($query, $year) {
                    return $query->whereYear('created_at', $year);
                });
        }
        return response([
            'visitors' => (int) $visitors->sum('daily_visitors'),
            'users' => $users->count(),
            'recipes' => $recipes->count(),
            'events' => $notifications->count(),
        ]);
    }

    /**
     * users admninistration and sorting.
     *
     * @return Response
     */
    public function usersFilter(Request $request)
    {
        $filtered_request = (object) array_filter($request->all());

        $group_by = $filtered_request->group ?? null;

        $where = [];

        if (isset($filtered_request->role)) {
            $data = ['role', $filtered_request->role];
            array_push($where, $data);
        }

        if (isset($filtered_request->email)) {
            if ($filtered_request->email == 1) {
                $data = ['email_verified_at', '!=', null];
            } elseif ($filtered_request->email == 0) {
                $data = ['email_verified_at', null];
            }
            array_push($where, $data);
        }

        return response([
            'users' => user::where($where)->when($group_by, function ($query, $group_by) {
                if ($group_by === 'newest') {
                    return $query->latest();
                } elseif ($group_by === 'oldest') {
                    return $query->oldest();
                }
            })->paginate(12),
        ]);
    }

    /**
     * users admninistration and sorting.
     *
     * @return Response
     */

    public function recipesFilter(Request $request)
    {
        $filtered_request = (object) array_filter($request->all());

        $group_by = $filtered_request->group ?? null;

        $where = [];

        if (isset($filtered_request->confirmation)) {
            $data = ['confirmation', (int) $filtered_request->confirmation];
            array_push($where, $data);
        }

        if (isset($filtered_request->user)) {
            if ($filtered_request->user == 0) {
                $data = ['user_id', null];
            } elseif ($filtered_request->user == 1) {
                $data = ['user_id', !null];
            }
            array_push($where, $data);
        }

        return response([
            'recipes' => recipe::where($where)->when($group_by, function ($query, $group_by) {
                if ($group_by === 'newest') {
                    return $query->latest();
                } elseif ($group_by === 'oldest') {
                    return $query->oldest();
                }
            })->with('user')->paginate(12),
        ]);
    }

    /**
     * users admninistration.
     *
     * @return Response
     */

    public function userAdministration(Request $request, $id)
    {
        $request->validate([
            'role'=>'string',Rule::in(['user', 'administrator']),
        ]);

        $user = user::find($id);

        $this->authorize('delete', $user);

        $user->update([
            'role' => $request->role,
        ]);
    }

    /**
     * Remove the specified user.
     *
     * @id  \App\Models\user  $user
     * @return Response
     */
    public function deleteUser(Request $request, $id)
    {
        $user=User::find($id);

        $this->authorize('delete', $user);

        Storage::deleteDirectory("public/images/users/$user->name/");

        $user->delete();

        return response()->json(['message'=> 'User deleted'], 200);
        
    }

    /**
     * recipe Administration.
     *
     * @id  \App\Models\user  $user
     * @return Response
     */

    public function recipeAdministration(Request $request, $id)
    {

        

        $request->validate([
            'confirmation' => 'integer',
        ]);

        $recipe = recipe::find($id);

        $recipe->update([
            'confirmation' => $request->confirmation,
        ]);
    }
}
