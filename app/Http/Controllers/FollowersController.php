<?php
namespace App\Http\Controllers;

use App\Notifications\notifyUser;
use App\Models\User;
use Illuminate\Http\Request;

class FollowersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $followers= User::find($request->id)->followers()->paginate(10);

        return response($followers, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $follower=auth()->user();

        $already_follow=$follower->followings()->where('id', $request->follower_id)->first();

        if (isset($already_follow)) {
            return response()->json([
                'message'=>'The given data was invalid. User already follows'
            ], 422);
        };

        $followed_user=User::find($request->follower_id);

        $follower->followings()->attach($followed_user) ;

        $url=url('/pouzivatel/' . $follower->name);

        $message=' vás začal sledovať';

        $data=[
            'message'=> $message,
            'icon'=>'avatar',
            'link'=> $url,
            'user'=> $request->user()
        ];

        $followed_user->notify(new notifyUser($data));

        return response($follower, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Follower  $follower
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        auth()->user()->followings()->detach($id) ;
        return response()->json(['message'=>'Deleted'], 200);
    }
}
