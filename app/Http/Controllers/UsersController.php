<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::where('id', $id)
            ->orWhere('name', $id)
            ->firstOrFail();

        if ($request->ajax()) {
            $filtered_request=(object) array_filter($request->all());

            $where=
            [
                ['confirmation', 1]
            ];

            if (isset($filtered_request->category)) {
                $category= ['category', $filtered_request->category];
                array_push($where, $category);
            };

            if (isset($filtered_request->search)) {
                $search= ['title', 'like', '%' . $filtered_request->search . '%'];
                array_push($where, $search);
            };
            return [
                'recipes'=> $user->recipes()->with('user')->where($where)->paginate(12),

            ];
        }

        $recipes = $user->recipes()->get();
        foreach ($recipes as $recipe) {
            foreach ($recipe->ratings as $rating) {
                $ratings[]=$rating->stars;
            }
        };
        if (isset($ratings)) {
            $ratings = array_sum($ratings)/count($ratings);
            $ratings = round($ratings, 1) ;
        } else {
            $ratings = 0;
        }

        return view('user', [
            'user'=> $user,
            'avg_ratings' => $ratings,
            'created_at' => $user->created_at
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @id  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        $user=User::find(auth()->id());

        return view('settings', [
            'user'=>$user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @id  \Illuminate\Http\Request  $request
     * @id  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        $this->authorize('update', $user);

        $request->validate([
            'name' => 'unique:users|max:255',
            'avatar' => 'image',
            'cover' => 'image',
            'email' => 'nullable',
            'status' => 'string | nullable',
        ]);

        if ($request->file('avatar')) {
            $random= mt_rand(1000000, 9999999);
            $name = $request->file('avatar')->getClientOriginalName();

            $request->file('avatar')->storeAs(
                'public/images/users/' . $user->name . '/',
                $random . $name
            );
            if ($user->avatar) {
                $image=$user->avatar;
                $path=explode('/', $image, );
                Storage::delete("public/images/users/$user->name/$path[7]");
            }
        }
        if ($request->file('cover')) {
            $random= mt_rand(1000000, 9999999);
            $name = $request->file('cover')->getClientOriginalName();

            $request->file('cover')->storeAs(
                'public/images/users/' . $user->name . '/',
                $random . $name
            );
            if ($user->cover) {
                $image=$user->cover;
                $path=explode('/', $image, );
                Storage::delete("public/images/users/$user->name/$path[7]");
            }
        }

        $data=$request->all();
        if ($request->file('avatar')) {
            $data['avatar'] = asset("storage/images/users/$user->name/$random$name");
        }
        if ($request->file('cover')) {
            $data['cover'] = asset("storage/images/users/$user->name/$random$name");
        }

        $user->update(
            $data
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $this->authorize('delete', $user);

        if (Hash::check($request->password, auth()->user()->password)) {

            Storage::deleteDirectory("public/images/users/$user->name/");

            $user->delete();

            return response()->json(['message'=> 'User deleted'], 200);
        } else {
            return response()->json(['message'=> 'Incorrect password'], 500);
        }
    }
}
