<?php
namespace App\Http\Controllers;

use App\Models\notification;
use App\Notifications\notifyUser;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $notification= auth()->user()->notifications()->latest()->paginate(20);
        return response()->json($notification, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function markAsRead(Request $request)
    {
        Notification::find($request->id)->update(['read_at' => now()]);

        return response()->json(['timestamp'=> now()], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(notification $notification)
    {
        $notification->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        auth()->user()->notifications()->delete();
    }
}
