<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
            'avatar' => ' image ',

        ]);

        if ($request->file('avatar')) {

            $random= mt_rand(1000000, 9999999);

            $name = $request->file('avatar')->getClientOriginalName();

            $request->file('avatar')->storeAs(
                'public/images/users/'.$request->name.'/',
                $random . $name
            );

            $avatar= asset('storage/images/users/'.$request->name.'/'.$random . $name);
        }
        else $avatar = asset('storage/images/users/Default/User-Profile.svg');

        Auth::login($user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'avatar'=> $avatar,
            'cover'=> asset('storage/images/users/Default/background.jpg')

        ]));

        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME);
    }
}
