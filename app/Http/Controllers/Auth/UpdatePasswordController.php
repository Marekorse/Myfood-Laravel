<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller
{
    public function update(Request $request)
    {
        $request->only('current_password', 'new_password', 'new_password_confirmation');

        $request->validate(
            [
                'current_password' => 'required|current_password',
                'new_password_confirmation' => 'required',
                'new_password' => 'required|string|min:8|confirmed|different:current_password',
            ],
            [
                'current_password.required' => 'Prosím vyplnte pole.',
                'current_password.current_password' => 'Zadali ste nesprávne heslo.',
                'new_password_confirmation.required' => 'Prosím vyplnte pole.',
                'new_password.required' => 'Prosím vyplnte pole.',
                'new_password.different' => 'Vaše nové heslo nesmie byť rovnaké ako staré.',
                'new_password.min' => 'Heslo musí mať minimálne 8 znakov.',
                'new_password.confirmed' => 'Heslo sa nezhoduje.'
            ]
        );

        $user = Auth::user();

        $user->password = Hash::make($request->new_password);
        
        $user->save();

        return response()->json('{"success":"password has been successfully changed"}', 200);
    }
}
