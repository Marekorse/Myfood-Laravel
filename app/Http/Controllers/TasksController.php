<?php
namespace App\Http\Controllers;

use App\Models\task;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'task_name' => 'required',
            'order' => 'integer',
            'todo_id' =>'integer'

        ]);

        $task=task::create([
            'task_name'=> $request->task_name,
            'todo_id' => $request->todo_id,
            'order' => $request->order,

        ]);
        return $task;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(task $task)
    {
        return $task;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, task $task)
    {
        if ($request->todo_id) {
            $task->update([
                'todo_id' => $request->todo_id,
            ]);
        };

        if ($request->task_name) {
            $task->update([
                'task_name' => $request->task_name,
            ]);
        };
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\task  $task
     * @return \Illuminate\Http\Response
     */
    public function reorder(Request $request)
    {
        $tasks = task::where('todo_id', $request->todo_id)->get();

        foreach ($tasks as $task) {
            $task->timestamps = false;
            $id = $task->id;
            foreach ($request->tasks as $new_task) {
                if ($new_task['id'] == $id) {
                    $task->update(['order' => $new_task['order']]);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(task $task)
    {
        $task->delete();
    }
}
