<?php
namespace App\Http\Controllers;

use App\Notifications\notifyUser;
use App\Models\like;
use Illuminate\Validation\Rule;
use App\Models\recipe;
use Illuminate\Http\Request;

class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $likes=recipe::find($request->recipe_id)->likes()->with('user')->paginate(10);

        return response($likes, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'recipe_id'=> Rule::unique('likes')->where(function ($query) use ($request) {
                return $query->where('user_id', $request->user()->id);
            })
        ]);

        $user=auth()->user();

        $like=$user->likes()->create(
            $request->only('recipe_id')
        )->load('user');

        $url=url('/recept/' . $request->recipe_id);

        $message=' sa páči váš recept';

        $data=[
            'message'=> $message,
            'icon'=>'avatar',
            'link'=> $url,
            'user'=> $user
        ];

        $to_user=recipe::find($request->recipe_id)->user()->first();

        if ($to_user) {
            $to_user->notify(new notifyUser($data));
        }

        return response($like, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, like $like)
    {
        $like->delete();
        
        return response(['message'=>'Deleted'], 200);
    }
}
