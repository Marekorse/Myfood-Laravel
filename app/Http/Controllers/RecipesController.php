<?php
namespace App\Http\Controllers;

use App\Models\recipe;

use App\Notifications\notifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Cache;
use function GuzzleHttp\json_decode;

class RecipesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $filtered_request=(object) array_filter($request->all());

            $where=[
                ['confirmation', 1]
            ];

            if (isset($filtered_request->category)) {
                $category= ['category', $filtered_request->category];
                array_push($where, $category);
            };

            if (isset($filtered_request->search)) {
                $search= ['title', 'like', '%' . $filtered_request->search . '%'];
                array_push($where, $search);
            };

            return [
                'recipes'=> recipe::where($where)->with('user')->withCount('comments', 'likes')->paginate(12),

            ];
        } else {
            return view('recipes');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auth= auth()->check();

        return view('add-recipe', [
            'auth'=>$auth
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title' => 'required|unique:recipes|max:255',
                'file' => 'required | image ',
                'time' => 'required | integer',
                'chunk' => 'required | integer',
                'difficulty' => 'required | string',
                'ingredients' => 'required|not_in:[]',
                'text' => 'required ',
                'approach' => 'required|not_in:[]',
                'guest' => 'string | nullable',
                'category' => 'required | string',
            ],
            [
                'title.required' => 'Prosím vyplnte nadpis receptu. ',
                'file.required' => 'Prosím nahrajte fotografiu vášho receptu. ',
                'time.required' => 'Prosím vyplnte čas prípravy. ',
                'chunk.required' => 'Prosím vyplnte počet porcii.',
                'difficulty.required' => 'Prosím vyplnte obtiažnosť receptu.',
                'text.required' => 'Prosím vyplnte popis receptu.',
                'category.required' => 'Prosím vyplnte kategoriu receptu.',
                'ingredients.not_in' => 'Prosím vyplnte ingrediencie receptu.',
                'approach.not_in' => 'Prosím vyplnte postup receptu.'

            ]
        );

        $random= mt_rand(1000000, 9999999);

        $name = $request->file('file')->getClientOriginalName();

        $request->file('file')->storeAs(
            'public/images/recipes/',
            $random . $name
        );

        $filtered_data=[
            'title'=>$request->title,
            'category'=>$request->category,
            'img' => asset('storage/images/recipes/' . $random . $name),
            'time'=>$request->time,
            'chunk'=>$request->chunk,
            'difficulty'=>$request->difficulty,
            'ingredients'=>$request->ingredients,
            'text'=>$request->text,
            'approach'=>$request->approach,
            'slug' => Str::slug($request->title),
            'link' => url('/recept/' . $request['slug']),
            'guest'=> $request->guest ?? 'neregistrovaný poúživateľ'
        ];

        $user=auth()->user();

        if ($user) {
            $filtered_data['confirmation'] = 1 ;
            $recipe=$user->recipes()->create($filtered_data);

            $notification=[
                'message'=> 'pridal nový recept',
                'icon'=>'avatar',
                'link'=> url('/recept' . $recipe->title),
                'user'=> $user
            ];

            $followers= $user->followers;
            Notification::send($followers, new notifyUser($notification));
        } else {
            $filtered_data['confirmation'] = 0 ;

            Recipe::create($filtered_data);
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\recipe  $recept
     * @return \Illuminate\Http\Response
     */
    public function show($param, Request $request)
    {
        $recipe = recipe::where('id', $param)
            ->orWhere('slug', $param)
            ->with('favourite', 'user')
            ->firstOrFail();

        $user=auth()->user();

        return view('recipe', [
            'user'=>$user,
            'recipe'=> $recipe,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\recipe  $recept
     * @return \Illuminate\Http\Response
     */
    public function edit(recipe $recept)
    {
        $this->authorize('update', $recept);

        $recept['ingredients']=json_decode($recept->ingredients);

        $recept['approach']=json_decode($recept->approach);

        return view('edit-recipe', [
            'recept'=> $recept,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\recipe  $recept
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, recipe $recept)
    {
        $this->authorize('update', $recept);

        $request->validate([
            'category'=>'required | string',
            'title' => "required|unique:recipes,title,{$recept->id}",
            'file' => ' image ',
            'time' => 'required | integer',
            'chunk' => 'required | integer',
            'difficulty' => 'required | string',
            'ingredients' => 'required | string',
            'text' => 'required',
            'approach' => 'required | string ',

        ]);

        if ($request->file('file')) {

            $random= mt_rand(1000000, 9999999);

            $name = $request->file('file')->getClientOriginalName();

            $request->file('file')->storeAs(
                'public/images/recipes/',
                $random . $name
            );
            if ($recept->img) {
                $image=$recept->img;
                $path=explode('/', $image, );
                Storage::delete('public/images/recipes/' . $path[5]);
            }
        }

        $filtered_data=[
            'title'=>$request->title,
            'category'=>$request->category,
            'time'=>$request->time,
            'chunk'=>$request->chunk,
            'difficulty'=>$request->difficulty,
            'ingredients'=> $request->ingredients,
            'text'=>$request->text,
            'approach'=> $request->approach,
            'slug' => Str::slug($request->title)
        ];
        if ($request->file('file')) {
            $filtered_data['img'] = asset('storage/images/recipes/' . $random . $name);
        }

        $recept->update(
            $filtered_data
        );

        return $recept;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\recipe  $recept
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, recipe $recept)
    {
        $this->authorize('delete', $recept);

        if ($recept->img) {
            $image=$recept->img;
            $path=explode('/', $image, );
            Storage::delete('public/images/recipes/' . $path[5]);
        }
        $recept->delete();
    }

    /**
     * Update recipe confirmation .
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\recipe  $recept
     * @return \Illuminate\Http\Response
     */
    public function confirm_recipe(Request $request, $id)
    {
        $recipe=recipe::find($id);

        $this->authorize('update', $recipe);

        $request->validate([
            'confirmation' => 'required | integer | max:1',
        ]);
        $recipe->update([
            'confirmation'=> $request->confirmation
        ]);
        return $recipe;
    }
}
