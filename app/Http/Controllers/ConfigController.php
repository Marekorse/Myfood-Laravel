<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ConfigController extends Controller
{
        public function resetConfig()
    {
       Artisan::call('config:cache');

       Artisan::call('route:cache');

       Artisan::call('view:cache');

       return ' done ';
    }

    public function link()
    {
       
      Artisan::call('storage:link', []);
   
      return 'link created';
    }
}
