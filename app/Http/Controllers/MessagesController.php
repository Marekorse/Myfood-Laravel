<?php
namespace App\Http\Controllers;

use App\Notifications\notifyUser;
use App\Models\message;
use App\Events\newMessage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Null_;
use PhpParser\Node\Stmt\Return_;

class MessagesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text'    => 'String|nullable',
            'conversation_id' => 'required| integer',
            'touser'=>'required| integer',
            'images.*'=> 'image',
        ]);

        if ($request->images) {
            $path=asset('storage/images/conversations/' . $request->conversation_id . '/');

            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            };

            $images_path=[];

            foreach ($request->images as $image) {
                $random= mt_rand(1000000, 9999999);
                $name = $image->getClientOriginalName();
                $image->storeAs(
                    'public/images/conversations/' . $request->conversation_id,
                    $random . $name
                );
                $images_path[]=$path . '/' . $random . $name;
            }
        };

        $newmessage= auth()->user()->messages()->create(
            [
                'conversation_id'=>$request->conversation_id,
                'text'=> $request->text,
                'images'=> json_encode($images_path ?? null),

            ]
        );

        $touser=User::find($request->touser);

        $fromuser=auth()->user();

        $message = 'vám poslal novú správu';

        $url= url('/spravy');

        $data=[
            'message'=>$message,
            'icon'=>'avatar',
            'user'=> $fromuser,
            'link'=>   $url
        ];
        
        $touser->notify(new notifyUser($data));

        broadcast(new newMessage($newmessage->load('user')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(message $message)
    {
        $message->delete();
    }
}
