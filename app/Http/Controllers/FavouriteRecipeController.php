<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class FavouriteRecipeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $filtered_request=(object) array_filter($request->all());
            $where=[
                ['confirmation', 1]
            ];

            if (isset($filtered_request->category)) {
                $category= ['category', $filtered_request->category];
                array_push($where, $category);
            };

            if (isset($filtered_request->search)) {
                $search= ['title', 'like', '%' . $filtered_request->search . '%'];
                array_push($where, $search);
            };
            return [
                'recipes'=> auth()->user()->favourites()->where($where)->with('user')->paginate(12),
            ];
        } else {
            return view('favourite-recipes');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(auth()->id());

        $user->favourites()->attach($request->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = User::find(auth()->id());
        
        $user->favourites()->detach($request->id);
    }
}
