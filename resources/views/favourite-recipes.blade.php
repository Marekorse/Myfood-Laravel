@extends('master')
@section('content')
<navigation-panel
    search-allowed
    :user="{{auth()->user() ?? 0}}"
></navigation-panel>
<div class="pt-16  h-screen">
        <h2 class="w-full custom-color-logoGray  pt-10 font-corinthia text-4xl font font-semibold text-center text-green-500 ">Moje oblúbené recepty</h2>
    <recipes-container
    url="{{url('/recepty/oblubene')}}"
    ></recipes-container> 
</div>  
@endsection