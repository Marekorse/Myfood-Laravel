@extends('master')
@section('content')
<navigation-panel
    search-allowed
    :user="{{auth()->user() ?? 0}}"
></navigation-panel>
<div class="flex flex-col justify-center pt-16 mx-auto bg-white h-full">
  <div class=" bg-white h-full ">
    <div class="relative ">
        <figure class="absolute top-56 left-1/2 transform -translate-x-1/2   w-32 h-32 rounded-full overflow-hidden  dark:border-white  border-4 border-white">
          <img class="w-full h-full object-cover" src="{{$user->avatar}}" alt="">
        </figure>  
        <figure class="flex items-center h-80  overflow-hidden ">
          <img class="w-full h-full object-cover" src="{{$user->cover}}" alt=" background">
        </figure> 
    </div>
    <div class="text-center p-4 pt-10">
      <div class="flex items-center justify-center">
        <h1 class="text-blue-400 group bg-white rounded-md inline-flex items-center text-xl font-semibold  ">{{$user->name}}</h1>
        <div class="list-none text-center ml-3">
          @if (auth()->id() && auth()->id() != $user->id)
            <new-message 
              small
              :user_id="{{$user->id}}"
            ></new-message>
            @else 
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>  
          @endif
        </div>
      </div>
      <p class="text-gray-400 text-sm font-medium">{{$user->status}}</p>
    </div>
    <div class="flex justify-center items-center mb-6 ">
      <div class="list-none text-center border-r-2 border-gray-500 mr-2 pr-2">
        <li class="font-semibold text-lg text-blue-400">{{$user->recipes->count()}}</li>
        <li>počet receptov</li>
      </div>
      <div class="list-none text-center border-r-2 border-gray-500 mr-2 pr-2 ">
        <li class="font-semibold text-lg  text-blue-400 flex justify-center items-center">
          {{$avg_ratings}} 
          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-yellow-300 self-start" viewBox="0 0 20 20" fill="currentColor">
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
          </svg>
        </li>
      <li>priemerné hodnotenie</li>
    </div>
      <div class="list-none text-center ">
        <li class="font-semibold text-lg text-blue-400">{{date_format($user->created_at, 'd m Y')}}</li>
        <li>registrovaný od </li>
      </div>
    </div>
    <recipes-container class=""
    user-id="{{$user->id}}"
    url="{{url('/pouzivatel/'.$user->name)}}"
    ></recipes-container>
  </div>
</div>
@endsection