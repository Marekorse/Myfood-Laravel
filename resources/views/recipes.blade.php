@extends('master')
@section('title', 'my food')
@section('content')
<navigation-panel
search-allowed
:user="{{auth()->user() ?? 0 }}"
></navigation-panel>
    <div class="pt-16">
      <recipes-container
      url="{{url('/recepty')}}"
      ></recipes-container> 
    </div>  
@endsection    
