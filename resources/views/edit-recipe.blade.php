@extends('master')
@section('content')
<navigation-panel
    :user="{{auth()->user() ?? 0}}"
></navigation-panel>
<recipe-form
:auth="{{auth()->id()}}"
:editable-recipe="{{$recept}}"
editing
with-redirect
><recipe-form>
    
@endsection